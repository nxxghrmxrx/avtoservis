﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Автосервис1
{
    public partial class Товары1 : Form
    {
        public Товары1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Меню меню = new Меню();
            меню.Show();
            Hide();
        }

        private void Товары1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.Product". При необходимости она может быть перемещена или удалена.
            this.productTableAdapter.Fill(this.автосервисDataSet.Product);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Validate();
            productBindingSource.EndEdit();
            productTableAdapter.Update(автосервисDataSet);
            productTableAdapter.Fill(автосервисDataSet.Product);
        }
    }
}
